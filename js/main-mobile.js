
tabs = (item,content_first,content) => {
    content_first.addClass('active');
    item.click(function () {
       item.removeClass('active');
       $(this).addClass('active');
       let cur = $(this).index();

       content.removeClass('active');
        console.log(content.eq(cur).addClass('active'));
       content.eq(cur).addClass('active');
    });
};

//Preloader
preloader = () =>  {
    let preloader = $('.preloader');
    $(window).on('load', function() {
        preloader.fadeOut(800);
        //AOS.init();
    });
};

bannerSlide = () => {
    $('.banner_slide').slick({
        arrows: false,
        dots: true,
    });
};

closeMenuMobile = () => {
    /* When user clicks outside */
    $(".menu_mobile_close").click(function() {
        $(this).removeClass('open');
        $('.bg_overlay').removeClass('open');
        $(".menu_mobile").removeClass("open");
        $('html , body').removeClass('open_menu');
    });
};

toggleHamburgerIcon  = () => {
    $('.header_hamburger_icon').click(function(){
        $(this).addClass('open');
        $('.menu_mobile_left').addClass('open');
        $('.bg_overlay').addClass('open');
        $('html , body').addClass('open_menu');
    });
};

toggleSubmenu = () => {
  let arrButton = $ ('.menu_mobile_arr');
  let backButton = $ ('.menu_mobile_back');
  arrButton.click(function () {
      $(this).parent().children('.menu_mobile_sub').addClass('open');
  });
    backButton.click(function () {
        console.log($(this).offsetParent());
       $(this).offsetParent().removeClass('open');
    });
};

toggleUserIcon  = () => {
    $('.header_user_icon').click(function(){
        $(this).addClass('open');
        $('.menu_mobile_right').addClass('open');
        $('.bg_overlay').addClass('open');
        $('html , body').addClass('open_menu');
    });
};

tabStore = () => {
    let item = $('.store_tab .tab_item');
    let content = $('.store_tab .tab_content_item');
    let content_first = $('.store_tab .tab_content_item:first-child');
    tabs(item,content_first,content);
};

tabSuggest = () => {
    let item = $('#tab_suggest .tab_item');
    let content = $('#tab_suggest .tab_content_item');
    let content_first = $('#tab_suggest .tab_content_item:first-child');
    tabs(item,content_first,content);
};

tabProductMain = () => {
    let item = $('.product_tab_item');
    let content = $('.product_main_tab .tab_content_item');
    let content_first = $('.product_main_tab .tab_content_item:first-child');
    tabs(item,content_first,content);
};

openOptionsPage = () => {
    $('.block_header_arrow').click(function () {
        $(this).toggleClass('active');

        $(document).mouseup(function (e) {
            if (!$(e.target).hasClass("block_header_arrow") && $(e.target).parents(".block_header_arrow").length === 0) {
                $(".block_header_arrow").removeClass('active');
            }
        });
    });
};

galleryProduct = () => {
    $('.gallery_full').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    });
};

toggleHeaderSelect = () => {
    let headerSelect = $('.header_select');
    let headerSelectOpen = $('.header_select.open');
    headerSelect.click(function () {
        $(this).toggleClass('open');
        $(this).siblings().removeClass('open');
    });
    headerSelectOpen.click(function () {
        console.log('aa');
    });
};

toggleFilter = () => {
  let headerFilter = $('.filter_header');
  headerFilter.click(function () {
      console.log('aa');
     $(this).parent().toggleClass('close');
  });
};
openPopupSearch = () => {
    $('.header_search').click(function () {
        $('.popup_container').addClass('open');
        $('.header_search_input').focus();
        $('html,body').css ('overflow','hidden');
    });
};
closePopupSearch = () => {
    $('.button_close_popup').click(function () {
        $('.popup_container').removeClass('open');
        $('html,body').css ('overflow','auto');
    });
};
openHeaderSearch = () => {
    $('.header_button_search').click(function () {
        $('.popup_container').addClass('open');
        $('html,body').css ('overflow','hidden');
    });
};


quantiyCountBox = () => {
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }
    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }
    $('.button_plus').click(function (e) {
        incrementValue(e);
    });
    $('.button_minus').click(function (e) {
        decrementValue(e);
    });
};

$(document).ready(function () {
    toggleHamburgerIcon();
    toggleUserIcon ();
    closeMenuMobile();
    bannerSlide();
    tabStore();
    tabSuggest ();
    openOptionsPage ();
    galleryProduct ();
    tabProductMain ();
    toggleSubmenu ();
    toggleHeaderSelect ();
    toggleFilter ();
    openPopupSearch();
    openHeaderSearch ();
    closePopupSearch ();
    quantiyCountBox ();
});