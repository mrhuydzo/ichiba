
tabs = (item,content_first,content) => {
    content_first.addClass('active');
    item.click(function () {
       item.removeClass('active');
       $(this).addClass('active');
       let cur = $(this).index();

       content.removeClass('active');
        console.log(content.eq(cur).addClass('active'));
       content.eq(cur).addClass('active');
    });
};
tabGoodDeal = () => {
    let item = $('#block_good_deals .tab_item');
    let content = $('#block_good_deals  .tab_content_item');
    let content_first = $('#block_good_deals  .tab_content_item:first-child');
    tabs(item,content_first,content);
};
tabStore = () => {
    let item = $('.store_tab .tab_item');
    let content = $('.store_tab .tab_content_item');
    let content_first = $('.store_tab .tab_content_item:first-child');
    tabs(item,content_first,content);
};

tabBank = () => {
    let item = $('.payment_type_tab_item a');
    let content = $('.payment_type_tab_content');
    let content_first = content.first();
    tabs(item,content_first,content);
};

productDetailTab = () => {
    let item = $('.product_detail_tab .tab_item');
    let content = $('.product_detail_tab .tab_content_item');
    let content_first = content.first();
    tabs(item,content_first,content);
};


//Preloader
preloader = () =>  {
    let preloader = $('.preloader');
    $(window).on('load', function() {
        preloader.fadeOut(800);
        //AOS.init();
    });
};

bannerSlide = () => {
    $('.banner_slide').slick({
        arrows: false,
        dots: true,
    });
};

rakutenCaroulsel = () => {
    $('.rakuten_caroulsel_lst').slick({
        dots: false,
        arrows: true,
        slidesToShow: 5,
        slidesToScroll: 2
    })
};
mycouponsCaroulsel = () => {
    $('.my_coupons_lst').slick({
        dots: true,
        arrows: false,
        slidesToShow: 6,
        slidesToScroll: 2
    })
};

galleryProduct = () => {
    $('.gallery_full').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.gallery_thumb_lst'
    });
    $('.gallery_thumb_lst').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.gallery_full',
        arrows: true,
        focusOnSelect: true
    });
};

menuFixRight = () => {
    $(window).scrollTop(0);
    let menuFix = $('.menu_fix');
    let stickyTop = menuFix.offset().top;
    if (stickyTop > 325) {
        menuFix.css({'position':'fixed','top': 0})
    }else {
        menuFix.css({'position':'absolute','top': 325})
    }

    console.log(stickyTop);
    $(window).scroll(function(){
        if ($(this).scrollTop() > stickyTop) {
            menuFix.css({'position':'fixed','top': 0})
        } else {
            menuFix.css({'position':'absolute','top': stickyTop})
        }
    });
};

selectBrowser = () => {
    let item = $('.addon_browser_item');
    let content_first = $('.addon_browser_data:first-child');
    let content = $('.addon_browser_data');
    tabs(item,content_first,content);
};


liveStreamSlide = () => {
    $('.livestream_lst').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });
};

couponYshoppingLst = () => {
    $('.coupon_yshopping_lst').slick({
        slidesToShow: 4,
        slidesToScroll: 2,
        arrows: false,
        dots: true
    });
};

editAddressFrm = () => {
    let editItem = $('.profile_addess_edit_btn');
    let addressItem = $('.profile_addess_item');
    editItem.click(function (e) {
        e.preventDefault();
        $('.profile_addess_item').removeClass('active');
        $(this).parents(addressItem).addClass('active');
    });
};

storeHorizontal = () => {
    $('.store_tab_list').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: false,
        variableWidth: true,
        arrows: true
    });
};

openSubcat = () => {
    let openClick = $('.categories_item_toggle');
    let categoriesSub = $('.categories_sub');
    openClick.click(function () {
        console.log('aa');
        $(this).parent().next().slideToggle();
        $(this).toggleClass('active');
    });
};
topNewsSlide = () => {
    $('.top_news_slide').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });
};

profileAuctionTab = () => {
    let item = $('#profile_auction_tab .tab_item');
    let content_first = $('#profile_auction_tab  .tab_item:first-child');
    let content = $('#profile_auction_tab .tab_content_item');
    tabs(item,content_first,content);
};
selectPaymentType = () => {
  let selectType = $('.payment_type_header');
  selectType.click(function () {
      $('.payment_type_header').removeClass('active');
      $(this).addClass('active');
      $('.payment_type_body').removeClass('active');
      $(this).parent().find('.payment_type_body').addClass('active');
  });
};

scrollToDes = () => {
    $('.product_short_des_link').click(function () {
        $('html, body').animate({
            scrollTop: $("#product_detail_tab").offset().top
        }, 500);
    });
};


quantiyCountBox = () => {
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }
    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }
    $('.button_plus').click(function (e) {
        incrementValue(e);
    });
    $('.button_minus').click(function (e) {
        decrementValue(e);
    });
};



tabPaymentSelector = () => {
    let item = $('.payment_type_selector--item');
    let content = $('.payment_type');
    let content_first = $('.payment_type:first-child');
    tabs(item,content_first,content);
};

$(document).ready(function () {
    tabGoodDeal();
    tabStore();
    bannerSlide();
    rakutenCaroulsel();
    mycouponsCaroulsel();
    galleryProduct();
    //menuFixRight();
    selectBrowser();

    tabBank();
    liveStreamSlide();
    couponYshoppingLst();
    editAddressFrm();
    storeHorizontal();
    productDetailTab();
    openSubcat();
    topNewsSlide();
    profileAuctionTab ();
    selectPaymentType();
    scrollToDes ();
    quantiyCountBox ();
    tabPaymentSelector();
});